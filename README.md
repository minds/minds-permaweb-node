# Minds Permaweb Node

Minds Permaweb Node is a Node JS / Express microservice that acts as a bridge to the Arweave Permaweb network.

## Installation

### Ubuntu (20.04)

To use with the minds stack, first cd into your Minds directory.

To install, clone via SSH or HTTP in the required folder.

```
# ssh
git clone git@gitlab.com:minds/minds-permaweb-node.git

# http
git clone https://gitlab.com/minds/minds-permaweb-node.git
```

Change your current directory to the newly cloned folder and install

```
cd minds-permaweb-node
npm i
```

Next up to configure the server, copy the .env.example to .env, and add in your Arweave private key (ARWEAVE_XPRIV)

## Usage

To run, simply use `node server.js`

It will expose the following endpoints on the given port:

* POST to /permaweb/getId - Dry run a transaction to get ID, but do not post to network.
* POST to /permaweb - Generate validate and dispatch a transaction from params.
* GET to /permaweb/:txid - Get a transaction by id.
* GET to / - health check

For additional information on parameters please see documentation in the server.js file.

## Helm

Helm charts folder is designed to be used with Minds deployment system, and enables remote writing of env variables to a kubernetes pod. 

## License

[AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html).

